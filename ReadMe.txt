JAVA NATIVE INTERFACE

Progarm to invoke a C++ code from java.

Reference: https://thenewcircle.com/static/bookshelf/java_fundamentals_tutorial/_java_native_interface_jni.html

Steps:
1. Declare a native function in java class.
	private static native int addInt(int a, int b);

2. Compile the java source
	javac a.java
	
3. Create the C/C++ Header file
	javah a

4. C++ implementation of function addInt. Include jni.h (available in java 
installation directory) and a.h (produced in step 3)
	"""
	#include<jni.h>
	#include "a.h"

	JNIEXPORT jint JNICALL Java_a_addInt
	  (JNIEnv *, jclass, jint a, jint b)
	{
	return a+b;
	}
	"""

5. Compile C++ code (this will produce "libaddInt.so" file, a shared object 
which can then be linked with other objects to form an executable).
	gcc -shared -fPIC -I/usr/lib/jvm/java-7-openjdk-amd64/include add.cpp -o libaddInt.so
	
6. Execute the java program with "libaddInt.so" in java.library.path
	java -Djava.library.path=. a


This document was written on system with
	Ubuntu 14.04
	Openjdk-7-jdk (java version "1.7.0_65")
	gcc (Ubuntu 4.8.2-19ubuntu1) 4.8.2