public class a
{
	static
	{
		System.loadLibrary("addInt");
	}

	private static native int addInt(int a, int b);

	public static void main(String args[])
	{
		System.out.println(addInt(1,3));
	}
}
