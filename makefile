java_home = "/usr/lib/jvm/java-7-openjdk-amd64/"

%.class : a.java
	javac a.java
a.h: a.class
	javah a
libaddInt.so : add.cpp
	gcc -shared -fPIC -I$(java_home)/include add.cpp -o libaddInt.so
compile : a.class a.h libaddInt.so
run : compile
	java -Djava.library.path=. a
clean:
	rm *class *.h *.so
